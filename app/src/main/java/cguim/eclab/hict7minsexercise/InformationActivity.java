package cguim.eclab.hict7minsexercise;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vpadn.ads.VpadnAdRequest;
import com.vpadn.ads.VpadnAdSize;
import com.vpadn.ads.VpadnBanner;

public class InformationActivity extends AppCompatActivity {
    ActionBar actionBar;
    private RelativeLayout adBannerLayout;
    private VpadnBanner vponBanner = null;
    private String bannerId = "8a80818247d3cb200147dd86c4cc0889";

    private final String text =
            "<body>" +
                    "<div style=\"text-align: center;\"><big>HICT 高強度循環訓練<br>" +
                    "</big></div>" +
                    "<br>" +
                    "<big style=\"text-decoration: underline;\">◎甚麼是HICT（高強度循環訓練）？</big><br>" +
                    "○傳統訓練 v.s. HICT<br>" +
                    "<big><big><font color=\"red\">傳統－</font></big></big><br>" +
                    "阻力訓練一般都與有氧訓練<font color=\"red\">分開進行</font>。<br>" +
                    "<font color=\"red\">一星期</font>間隔進行<font color=\"red\">2～3天</font>，" +
                    "中間可休息。<br>" +
                    "每一組的肌肉訓練，要做<font color=\"red\">8至12下</font>，並" +
                    "<font color=\"red\">重覆2至4次</font>。<br>" +
                    "美國醫學學會(ACSM)建議，依每個人的訓練程度，針對主要的肌肉群，將訓練動作的強度設在<font color=\"red\">1RM" +
                    "(所能舉的最大重量)的40%~80%</font>，<font color=\"red\">進行8~12次</font>，每個肌肉群" +
                    "建議作<font color=\"red\">2~4組</font>。中間<font color=\"red\">休息" +
                    "2~3分鐘</font>，適當恢復後再繼續。<br>" +
                    "<br>" +
                    "有氧運動的建議，則是<font color=\"red\">每週要累積150分鐘</font>，" +
                    "<font color=\"red\">每次的運動要持續30~60分鐘</font>，強度在「" +
                    "<font color=\"red\">中度</font>(最大攝氧量的46%~63%)」或者「" +
                    "<font color=\"red\">高強度</font>(最大攝氧量的64%~90%)」，每次運動需持續20~60分鐘左右。<br>" +
                    "<br>" +
                    "<big><big><font color=\"red\">HICT－</font></big></big><br>" +
                    "高強度循環訓練(High-Intensity-Circuit-Training, 簡稱HICT)」。<br>" +
                    "以徒手方式，打破傳統上各動作獨立、重覆進行的模式，<font color=\"red\">同時結合有氧及阻力訓練</font>，整套" +
                    "動作僅需持續「<font color=\"red\">7分鐘</font>」。<br>" +
                    "結合了帶氧運動與阻力運練的好處，<font color=\"red\">混合定時的帶氧運動</font>(aerobic" +
                    "exercise)與<font color=\"red\">阻力訓練</font>(resistance" +
                    "training)，是一種利用<font color=\"red\">自身體重</font>進行高強度的肌肉訓練。<br>" +
                    "<br>" +
                    "其特色在於：<br>" +
                    "－利用不同地方現有的道具進行運動<br>" +
                    "－輕易轉換不同動作，只需很少時間休息<br>" +
                    "－增強身體主要肌肉力量，並平衡全身力量<br>" +
                    "－利用大肌肉組，製造合適的阻力與有氧運動強度<br>" +
                    "－能即時改變動作強度<br>" +
                    "－安全且適合大部份人<br>" +
                    "<br>" +
                    "<br>" +
                    "<big><big><big>◎如何實踐？</big></big></big><br>" +
                    "<big style=\"color: red;\"><big>一個有效率的HICT</big></big><br>" +
                    "1.&nbsp;&nbsp;&nbsp; 動作順序(Exercise Order)<br>" +
                    "<font color=\"blue\">拮抗肌肉群交替安排</font><br>" +
                    "比方說，伏地挺身(上半身)之後，接深蹲(下半身)。進行伏地挺身時，下半身使用的比例較少，可以進行休息恢復，使下半身有足夠的能量，在足夠的強度下，" +
                    "以適當的姿勢及技術完成深蹲動作。<br>" +
                    "<br>" +
                    "<font color=\"blue\">心跳率高低交替</font><br>" +
                    "當某一特定動作，將使民眾心跳率大幅提高，或是動作的強度較高時(通常為整合下半身或全身的動態動作)，下個動作的功能便要選擇能降低其心跳率，或是強度" +
                    "較輕微的動作。比方說，一個Jumping Squat (跳躍深蹲)之後，接一個靜態的棒式(Plank)或是捲腹的動作(Crunches)。<br>" +
                    "目的在於讓被訓練的人，可以維持適當的姿勢及技術，在高強度及有限的休息時間下，快速完成一連串的動作。<br>" +
                    "<br>" +
                    "2.&nbsp;&nbsp;&nbsp; 動作個數(Number of Exercises)<br>" +
                    "原始的循環訓練要求9~12個動作，但沒有一個最佳的個數。重點是，在一個HICT，每個肌肉都可以在合適的強度下進行訓練。個數愈多，也會影響總共的時" +
                    "間。<br>" +
                    "<br>" +
                    "3.&nbsp;&nbsp;&nbsp; 每個動作的時間(Individual Exercise Bout Time)<br>" +
                    "為了將運動對身體所造成的代謝影響極大化，每個動作的時間應足以進行15~20次。「30秒」的時間即相當充裕，大部份的民眾已可從中獲得足夠的運動強" +
                    "度。也可透過外在的器材（如心跳錶）來了解民眾對心跳率及對於動作強度的反應。<br>" +
                    "<br>" +
                    "4.&nbsp;&nbsp;&nbsp; 動作與動作間的休息時間(Rest Between Exercise Bouts)<br>" +
                    "HICT的目的在於<font color=\"red\">有限的時間</font>內，在身體沒有得到完全的休息下，對身體代謝產生出最" +
                    "大的效益。30秒的休息或是更短已被認為是最佳的時間。若要將時間效率提升到最大，<font color=\"red\">建議休息的時間" +
                    "在「15秒」以內</font>。若休息的時間完，再進行動作時無法保持適當的姿勢，則代表休息時間過短。<br>" +
                    "<br>" +
                    "5.&nbsp;&nbsp;&nbsp; 總共訓練的時間(Total Exercise Time)<br>" +
                    "ACSM建議至少進行20分鐘，你也可以選擇進行多組的循環。<br>" +
                    "每組動作需在30秒內做15至20下，然後休息最多15秒，以保持足夠的訓練強度。<br>" +
                    "<br>" +
                    "&lt;基本動作&gt;<br>" +
                    "如開合跳、無影凳、掌上壓、仰卧起坐、踏凳、蹲伏、三頭肌撐體、撐體、原地提膝踏步、弓步、掌上壓後轉身、側身撐體等。<br>" +
                    "<big style=\"text-decoration: underline;\">執行成效</big><br>" +
                    "－增強心肺功能，並減低疲累感<br>" +
                    "－可代替長跑與舉重<br>" +
                    "－研究指出兒茶酚胺(catecholamine)和生長激素在血液的水平，在進行「七分鐘運動」時，都顯著增加。（這兩種物質都能快速、有效減去皮下脂" +
                    "肪。）<br>" +
                    "－運用身體大部份的肌肉組與短暫休息時間，可增強心肺功能、<font color=\"red\">促進新陳代謝</font>。" +
                    "<font color=\"red\">效用更可持續72小時</font>。<br>" +
                    "－另外，該運動亦會<font color=\"red\">減少胰島素抗性</font>(insulin" +
                    "resistance)（該抗性會導致代謝綜合症、痛風和糖尿病。）<br>" +
                    "<br>" +
                    "</body>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
//        actionBar = getActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView textview = (TextView)findViewById(R.id.infotext);
        textview.setText(Html.fromHtml(text));

        adBannerLayout = (RelativeLayout) findViewById(R.id.adLayout);
        vponBanner = new VpadnBanner(this, bannerId, VpadnAdSize.SMART_BANNER,
                "TW");

        VpadnAdRequest adRequest = new VpadnAdRequest();
        adRequest.setEnableAutoRefresh(true);
        vponBanner.loadAd(adRequest);

        adBannerLayout.addView(vponBanner);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode == KeyEvent.KEYCODE_BACK){
            this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (vponBanner != null) {
            vponBanner.destroy();
            vponBanner = null;
        }
    }
}
