package cguim.eclab.hict7minsexercise;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class RecordActivity extends AppCompatActivity {

    SQLiteDatabase db;
    ListView listview01;
    Cursor cursor;

    DBHelper helper = new DBHelper(RecordActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        listview01 = (ListView) findViewById(R.id.listView1);

        db = helper.getReadableDatabase();

        String sql = "SELECT * FROM " + DBHelper.TABLE_NAME + " ORDER BY "
                + DBHelper.ID + " DESC";

        Log.d("TAG", "sql:" + sql);

        cursor = db.rawQuery(sql, null);

        Log.d("hahaha", String.valueOf(cursor.getCount()));

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] {DBHelper.ID, DBHelper.DATE },
                new int[] {android.R.id.text1, android.R.id.text2 },
                0);
        listview01.setAdapter(adapter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    // 返回建
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            cursor.close();
//            Intent myIntent = new Intent();
//            myIntent = new Intent(RecordActivity.this, HICT7minsExercise.class);
//            startActivity(myIntent);
            this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
