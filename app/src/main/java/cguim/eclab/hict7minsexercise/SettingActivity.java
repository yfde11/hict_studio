package cguim.eclab.hict7minsexercise;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.KeyEvent;
import android.view.View;


@SuppressLint("NewApi")
public class SettingActivity extends PreferenceActivity implements OnPreferenceChangeListener,
        OnPreferenceClickListener{
    ActionBar actionBar;

    String KeepScreenOn;
    CheckBoxPreference KeepScreenOnCheckPref;

    String KeepSound;
    CheckBoxPreference HaveSoundCheckPref;

    String exercisetimekey;
    ListPreference exercisetimepref;

    String resttimekey;
    ListPreference resttimepref;

    String repeattimekey;
    ListPreference repeattimepref;



    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        //從xml文件新增preference項
        //螢幕亮度
//		KeepScreenOn = getResources().getString(R.string.keep_screenon_switch_key);
//		KeepScreenOnCheckPref = (CheckBoxPreference)findPreference(KeepScreenOn);
//		KeepScreenOnCheckPref.setOnPreferenceClickListener(this);
//		KeepScreenOnCheckPref.setOnPreferenceChangeListener(this);

        //計時音
//		KeepSound = getResources().getString(R.string.keep_timesoundon_switch_key);
//		HaveSoundCheckPref = (CheckBoxPreference)findPreference(KeepSound);
//		HaveSoundCheckPref.setOnPreferenceClickListener(this);
//		HaveSoundCheckPref.setOnPreferenceChangeListener(this);

        //運動時間長度
        exercisetimekey = getResources().getString(R.string.exercise_time_key);
        exercisetimepref = (ListPreference)findPreference(exercisetimekey);
        exercisetimepref.setOnPreferenceClickListener(this);
        exercisetimepref.setOnPreferenceChangeListener(this);
        exercisetimepref.setSummary(exercisetimepref.getEntry());

        //休息時間長度
        resttimekey = getResources().getString(R.string.rest_time_key);
        resttimepref = (ListPreference)findPreference(resttimekey);
        resttimepref.setOnPreferenceClickListener(this);
        resttimepref.setOnPreferenceChangeListener(this);
        resttimepref.setSummary(resttimepref.getEntry());
        //重複次數
//		repeattimekey = getResources().getString(R.string.repeat_time_key);
//		repeattimepref = (ListPreference)findPreference(repeattimekey);
//		repeattimepref.setOnPreferenceClickListener(this);
//		repeattimepref.setOnPreferenceChangeListener(this);
//		repeattimepref.setSummary(repeattimepref.getEntry());


        getListView().setBackgroundColor(Color.rgb(255, 250, 204));
        getListView().setCacheColorHint(Color.rgb(255, 250, 204));

    }

    @Override
    //返回建
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if(keyCode == KeyEvent.KEYCODE_BACK){
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }



    @Override
    public boolean onPreferenceClick(Preference preference) {
        // TODO Auto-generated method stub
        return false;
    }



    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        // TODO Auto-generated method stub
        if(preference.getKey().equals(exercisetimekey)) //根據不同的key來辨別不同的Preference
            preference.setSummary(newValue +"秒" );
        else if(preference.getKey().equals(resttimekey)) //根據不同的key來辨別不同的Preference
            preference.setSummary(newValue +"秒" );

        return true;
    }
    protected void onBindView(View view){

        view.setBackgroundColor(Color.rgb(255, 250, 204));
    }

}
