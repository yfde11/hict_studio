package cguim.eclab.hict7minsexercise;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class teachActivity extends Activity {

    String mov1 = "https://www.youtube.com/watch?v=OaXhiTMCQvs";
    String mov2 = "https://www.youtube.com/watch?v=6nk90kXPhzQ";
    String mov3 = "https://www.youtube.com/watch?v=e6PLo_ndSck";
    String mov4 = "https://www.youtube.com/watch?v=wIYy_uCPq_U";
    String mov5 = "https://www.youtube.com/watch?v=dGNjXIptKqw";
    String mov6 = "https://www.youtube.com/watch?v=dIMGlHtGnAM";
    String mov7 = "https://www.youtube.com/watch?v=qfPs0cEwb6M";
    String mov8 = "https://www.youtube.com/watch?v=L1xy7_wdpzw";
    String mov9 = "https://www.youtube.com/watch?v=TV5tkdviXpg";
    String mov10 = "https://www.youtube.com/watch?v=iLjMQDZYtZM";
    String mov11 = "https://www.youtube.com/watch?v=zdjnDfg97s0";
    String mov12 = "https://www.youtube.com/watch?v=bojiYFtYHFA";

    Button mov01 ;
    Button mov02 ;
    Button mov03 ;
    Button mov04 ;
    Button mov05 ;
    Button mov06 ;
    Button mov07 ;
    Button mov08 ;
    Button mov09 ;
    Button mov010 ;
    Button mov011 ;
    Button mov012 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach);

        mov01 = (Button) findViewById(R.id.button2);
        mov02 = (Button) findViewById(R.id.button3);
        mov03 = (Button) findViewById(R.id.button4);
        mov04 = (Button) findViewById(R.id.button5);
        mov05 = (Button) findViewById(R.id.button6);
        mov06 = (Button) findViewById(R.id.button7);
        mov07 = (Button) findViewById(R.id.button8);
        mov08 = (Button) findViewById(R.id.button9);
        mov09 = (Button) findViewById(R.id.button10);
        mov010 = (Button) findViewById(R.id.button11);
        mov011 = (Button) findViewById(R.id.button12);
        mov012 = (Button) findViewById(R.id.button13);

        mov01.setOnClickListener(listener);
        mov02.setOnClickListener(listener);
        mov03.setOnClickListener(listener);
        mov04.setOnClickListener(listener);
        mov05.setOnClickListener(listener);
        mov06.setOnClickListener(listener);
        mov07.setOnClickListener(listener);
        mov08.setOnClickListener(listener);
        mov09.setOnClickListener(listener);
        mov010.setOnClickListener(listener);
        mov011.setOnClickListener(listener);
        mov012.setOnClickListener(listener);


    }
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.button2:
                    youtube(mov1);
                    break;
                case R.id.button3:
                    youtube(mov2);
                    break;
                case R.id.button4:
                    youtube(mov3);
                    break;
                case R.id.button5:
                    youtube(mov4);
                    break;
                case R.id.button6:
                    youtube(mov5);
                    break;
                case R.id.button7:
                    youtube(mov6);
                    break;
                case R.id.button8:
                    youtube(mov7);
                    break;
                case R.id.button9:
                    youtube(mov8);
                    break;
                case R.id.button10:
                    youtube(mov9);
                    break;
                case R.id.button11:
                    youtube(mov10);
                    break;
                case R.id.button12:
                    youtube(mov11);
                    break;
                case R.id.button13:
                    youtube(mov12);
                    break;
            }

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.teach, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void youtube(String mov){
        Intent intent = new Intent(Intent.ACTION_VIEW, android.net.Uri.parse(mov));
        intent.setPackage("com.google.android.youtube");
        intent.putExtra("force_fullscreen",true);
        startActivity(intent);
    }
}
